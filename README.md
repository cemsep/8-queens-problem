﻿# 8 Queens Problem

## Description

A #C console application that allows the user to place a queen on a chess board and solves the 8 queens problem.

## Contributors

Cem Pedersen     @cemsep     https://gitlab.com/cemsep