﻿using System;
using System.Text.RegularExpressions;

namespace _8queensProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = 8;
            int[,] board = new int[8, 8];

            void InitializeBoard(int[,] board)
            {
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        board[i, j] = 0;
                    }
                }
            }

            void DisplayBoard(int[,] board)
            {
                Console.WriteLine("    a   b   c   d   e   f   g   h");
                Console.WriteLine("  ---------------------------------");

                for (int i = 0; i < N; i++)
                {
                    Console.Write(N - i + " ");

                    for (int j = 0; j < N; j++)
                    {
                        if (j == 7 && board[i, j] == 0)
                            Console.Write("| " + " " + " |");
                        else if(j == 7 && board[i, j] == 1)
                            Console.Write("| " + "Q" + " |");
                        else if(board[i, j] == 0)
                            Console.Write("| " + " " + " ");
                        else
                            Console.Write("| " + "Q" + " ");
                    }
                    Console.WriteLine();
                    Console.WriteLine("  ---------------------------------");
                }
            }

            bool CheckPositionAvailable(int[,] board, int row, int col)
            {
                int i, j;

                // check row on right side
                for (i = col; i < N; i++)
                {
                    if (board[row, i] == 1 && i != col)
                        return false;
                }

                // check row on left side
                for (i = col; i >= 0; i--)
                {
                    if (board[row, i] == 1 && i != col)
                        return false;
                }

                // check column on lower side
                for (i = row; i < N; i++)
                {
                    if (board[i, col] == 1 && i != row)
                        return false;
                }

                // check column on upper side
                for (i = row; i >= 0; i--)
                {
                    if (board[i, col] == 1 && i != row)
                        return false;
                }

                // check upper diagonal on right side
                for (i = row, j = col; i < N && j < N; i++, j++)
                {
                    if (board[i, j] == 1 && i != row && j != col)
                        return false;
                }

                // check upper diagonal on left side
                for (i = row, j = col; i < N && j >= 0; i++, j--)
                {
                    if (board[i, j] == 1 && i != row && j != col)
                        return false;
                }

                // check lower diagonal on right side
                for (i = row, j = col; i >= 0 && j < N; i--, j++)
                {
                    if (board[i, j] == 1 && i != row && j != col)
                        return false;
                }

                // check lower diagonal on left side
                for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
                {
                    if (board[i, j] == 1 && i != row && j != col)
                        return false;
                }
  
                return true;
            }

            bool Solve(int[,] board, int col)
            {
                if (col == N)
                    return true;

                for (int i = 0; i < N; i++)
                {
                    if (board[i, col] == 1)
                    {
                        if (CheckPositionAvailable(board, i, col))
                        {
                            if (Solve(board, col + 1) == true)
                                return true;

                            return false;
                        }
                    }
                    else if (CheckPositionAvailable(board, i, col))
                    {
                        board[i, col] = 1;
                        if (Solve(board, col + 1) == true)
                            return true;

                        board[i, col] = 0;
                    }
                }
                return false;
            }

            (int, int) ParseCoords(string position)
            {
                int row = N - (int)Char.GetNumericValue(position[1]);
                int col;

                switch (position[0])
                {
                    case 'a':
                        col = 0;
                        break;
                    case 'b':
                        col = 1;
                        break;
                    case 'c':
                        col = 2;
                        break;
                    case 'd':
                        col = 3;
                        break;
                    case 'e':
                        col = 4;
                        break;
                    case 'f':
                        col = 5;
                        break;
                    case 'g':
                        col = 6;
                        break;
                    default:
                        col = 7;
                        break;
                }

                return (row, col);
            }

            string GetUserInput()
            {
                bool isValid;
                string input;
                Regex regex = new Regex(@"(^[a-hA-H]+[1-8]$)");
                input = Console.ReadLine();
                isValid = regex.IsMatch(input);

                while (!isValid)
                {
                    Console.WriteLine("Invalid input! The input must be a chess coordinate! (example a7)");
                    input = Console.ReadLine();
                    isValid = regex.IsMatch(input);
                }

                return input;
            }

            bool PlaceQueen(int[,] board)
            {
                Console.WriteLine("Place a queen at a given chess coordinate (example a7):");

                (int row, int col) = ParseCoords(GetUserInput());

                board[row, col] = 1;

                if (!Solve(board, 0))
                {
                    Console.WriteLine("No solution found! Please try agian:");
                    board[row, col] = 0;
                    return false;
                }
                Console.WriteLine("============= SOLUTION =============");
                DisplayBoard(board);

                return true;
            }

            InitializeBoard(board);
            DisplayBoard(board);
            while (!PlaceQueen(board))
            {

            }
           
        }
    }
}
